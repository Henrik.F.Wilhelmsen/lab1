package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    Random rnd = new Random();
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    boolean playing = true;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {

        while (playing){
            System.out.printf("Let's play round %d\n", roundCounter);
            String human_choice = user_choice();
            String computer_choice = random_choice(rpsChoices);
            String choice_string = String.format("Human chose %s, computer chose %s.", human_choice, computer_choice);

            if (is_winner(human_choice, computer_choice)){
                System.out.printf("%s Human wins.\n", choice_string);
                humanScore++;
            }
            else if (is_winner(computer_choice, human_choice)){
                System.out.printf("%s Computer wins.\n", choice_string);
                computerScore++;
            }
            else{
                System.out.printf("%s It's a tie.\n", choice_string);
            }

            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);

            playing = continue_playing();
            roundCounter++;
        }

        System.out.println("Bye bye :)");
    }

    public String readInput(String prompt) {
        System.out.println(prompt);
        return sc.next();
    }

    public boolean validate_input(String inp, List<String> valid){
        String input = inp.toLowerCase();
        return valid.contains(input.toLowerCase());
    }

    public String random_choice(List<String> choices){
        return choices.get(rnd.nextInt(choices.size()));
    }

    public boolean is_winner(String choice1, String choice2){
        if (choice1.equals("paper")){
            return choice2.equals("rock");
        }
        else if (choice1.equals("scissors")){
            return choice2.equals("paper");
        }
        else{
            return choice2.equals("scissors");
        }
    }

    public String user_choice(){
        while (true){
            String user_inp = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (validate_input(user_inp, rpsChoices)){
                return user_inp;
            }
            else{
                System.out.printf("I don't understand %s. Could you try again?\n", user_inp);
            }
        }
    }

    public Boolean continue_playing(){
        while (true){
            String continue_answer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (validate_input(continue_answer, Arrays.asList("y", "n"))) {
                return continue_answer.equals("y");
            }
            else{
                System.out.printf("I don't understand %s. Try again\n", continue_answer);
            }
        }
    }
}
